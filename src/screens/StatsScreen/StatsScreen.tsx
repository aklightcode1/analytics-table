import React, { useState, useEffect, useMemo } from "react";
// import axios from "axios";
import styled from "styled-components";

import StatsTable from "../../components/StatsTable/StatsTable";
import StatsTableShowCell from "../../components/StatsTable/StatsTableShowCell";
import StatsTableDeleteCell from "../../components/StatsTable/StatsTableDeleteCell";
import StatsTableRankCell from "../../components/StatsTable/StatsTableRankCell";
import StatsTableExploreCell from "../../components/StatsTable/StatsTableExploreCell";

import serverResponse from "../../mocks/serverResponseMocks.json";
// import ApiURL from "../../config/api/api";

const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      max-width: 50px;
      min-width: 30px;
      text-align: center;

      :last-child {
        border-right: 0;
      }
    }
  }
`;

const StatsScreen: React.FC = () => {
  const [tableItems, setTableItems] = useState<any>({});

  const onDelete = (keyword: string) => {
      
    console.log("Deleted item", keyword);
  };

  const columns = useMemo(
    () => [
      {
        Header: "Keyword",
        accessor: "keyword"
      },
      {
        Header: " ",
        accessor: "",
        Cell: (props: any) => <StatsTableExploreCell cell={props.cell} />
      },
      {
        Header: "  ",
        accessor: "",
        Cell: props => <StatsTableShowCell cell={props.cell} />
      },
      {
        Header: "Traffic Score",
        accessor: "users_per_day"
      },
      {
        Header: "Rank",
        accessor: "",
        Cell: (props: any) => <StatsTableRankCell cell={props.cell} />
      },
      {
        Header: "Total apps",
        accessor: "total_apps"
      },
      {
        Header: "Color",
        accessor: "color"
      },
      {
        Header: "   ",
        accessor: "",
        Cell: ({ cell }) => (
          <StatsTableDeleteCell onDelete={onDelete} cell={cell} />
        )
      }
    ],
    []
  );

  const fetchTableItems = async () => {
    try {
      //   const tableItems = await axios.post(ApiURL.apiURL);
    //   let iPhoneItems = serverResponse.data.map(item => {
    //     if (item.isAppstore) return item;
    //   });
      setTableItems(serverResponse);
    } catch (err) {
      console.log("Error while fetching table items:", err);
    }
  };

  useEffect(() => {
    fetchTableItems();
  });

  useEffect(() => {}, []);

  return (
    <div>
      <Styles>
        <StatsTable columns={columns} tableItems={tableItems.data}>
          {" "}
        </StatsTable>
      </Styles>
    </div>
  );
};

export default StatsScreen;
