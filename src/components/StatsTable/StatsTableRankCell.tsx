import React from "react";

interface StatsTableRankCellProps {
  cell: {
    row: {
      original: {
        position_info: {
          position: number;
          change: number;
        };
      };
    };
  };
}

const StatsTableRankCell: React.FC<StatsTableRankCellProps> = props => {
  const { position, change } = props.cell.row.original.position_info;
  return (
    <div>
      {position}{" "}
      <span style={change > 0 ? { color: "green" } : { color: "red" }}>
        {change ? `(${change})` : ""}
      </span>
    </div>
  );
};

export default StatsTableRankCell;
