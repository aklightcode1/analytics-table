import React from "react";

interface StatsTableDeleteCell {
  cell: {
    row: {
      original: {
        keyword: "string";
      };
    };
  };
  onDelete(keyword: string): void;
}

const StatsTableDeleteCell: React.FC<StatsTableDeleteCell> = ({
  cell,
  onDelete
}) => {
  const { keyword } = cell.row.original;

  return (
    <div>
      <i
        className="material-icons red-text"
        style={{ cursor: "pointer" }}
        onClick={() => onDelete(keyword)}
      >
        delete
      </i>
    </div>
  );
};

export default StatsTableDeleteCell;
