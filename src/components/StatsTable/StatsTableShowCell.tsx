import React from "react";

interface StatsTableShowCellProps {
  cell: {
    row: {
      original: {
        suggestions_count: number;
      };
    };
  };
}

const StatsTableShowCell: React.FC<StatsTableShowCellProps> = props => {
  const suggestions_count = props.cell.row.original.suggestions_count;
  return (
    <>
      <a
        className="btn grey darken-1 modal-trigger"
        href="#modal1"
        style={{ fontSize: "11px" }}
      >
        Show
        {typeof suggestions_count === "number" ? `(${suggestions_count})` : ""}
      </a>
      <div id="modal1" className="modal">
        <div className="modal-content" style={{ minHeight: "200px" }}></div>
        <div className="modal-footer">
          <a
            href="#!"
            className="modal-close waves-effect waves-green btn-flat"
          >
            Close
          </a>
        </div>
      </div>
    </>
  );
};

export default StatsTableShowCell;
