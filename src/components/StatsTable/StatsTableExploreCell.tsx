import React from 'react'
import { useHistory } from "react-router-dom";

interface StatsTableExploreCellProps {
    cell: {
      row: {
        original: {
          keyword: string;
        };
      };
    };
  }

const StatsTableExploreCell: React.FC<StatsTableExploreCellProps> = (props) => {
    const history = useHistory();
    
    const keyword = props.cell.row.original.keyword;
    return (
      <a
        className="btn grey darken-1"
        onClick={() => history.push(`/explore/${keyword}`)}
        style={{fontSize: '11px'}}
      >
        Explore
      </a>
    );
}

export default StatsTableExploreCell