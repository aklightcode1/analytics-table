import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import StatsScreen from "./screens/StatsScreen/StatsScreen";
import ExploreScreen from "./screens/ExploreScreen/ExploreScreen";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <div className="container">
        <Switch>
          <Route component={StatsScreen} path="/" exact></Route>
          <Route component={StatsScreen} path="/stats" exact></Route>
          <Route
            component={ExploreScreen}
            path="/explore/:keyword"
            exact
          ></Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default App;
